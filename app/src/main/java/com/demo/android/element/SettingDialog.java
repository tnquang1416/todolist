package com.demo.android.element;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.demo.android.todolist.MainActivity;
import com.demo.android.todolist.R;
import com.demo.android.utils.PreferenceUtils;

import static android.content.Context.MODE_PRIVATE;

/**
 * Author: Quang Tran
 * Created on 5/12/2016.
 */

public class SettingDialog {
  private MainActivity m_activity;
  private SharedPreferences m_preferences;

  public SettingDialog(MainActivity activity) {
    m_activity = activity;
    m_preferences = m_activity.getSharedPreferences(PreferenceUtils.PREF_NAME, MODE_PRIVATE);
  }

  public void show() {
    AlertDialog.Builder builder = new AlertDialog.Builder(m_activity);
    final View dialogView = m_activity.getLayoutInflater().inflate(R.layout.todo_settings, null);
    builder.setTitle(R.string.action_settings);
    builder.setView(dialogView);
    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        RadioGroup group = (RadioGroup) dialogView.findViewById(R.id.option_group_show);
        int selectedOption = group.getCheckedRadioButtonId();
        int selectValue;
        saveSelection(selectedOption);
        switch (selectedOption) {
          case R.id.option_show_all:
            selectValue = PreferenceUtils.LOAD_ALL_TODO;
            break;
          case R.id.option_show_today:
            selectValue = PreferenceUtils.LOAD_ALL_TODO_TODAY;
            break;
          case R.id.option_show_from_today:
            selectValue = PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY;
            break;
          case R.id.option_show_today_pending:
            selectValue = PreferenceUtils.LOAD_ALL_TODO_TODAY_PENDING;
            break;
          case R.id.option_show_overdue:
            selectValue = PreferenceUtils.LOAD_OVERDUE_TODO;
            break;
          default:
            selectValue = PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY;
        }
        m_activity.onPrefSettingChange(PreferenceUtils.LOAD_MODE_LABEL, selectValue);
      }

      // Should use listener or observer to notify the list item
    });
    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        // Do nothing
      }
    });
    initSelection(dialogView);
    builder.show();
  }

  private void initSelection(View dialogView) {
    int mode = PreferenceUtils.getLoadMode(m_preferences);

    switch (mode) {
      case PreferenceUtils.LOAD_ALL_TODO:
        checkRadioButton(dialogView, R.id.option_show_all);
        break;
      case PreferenceUtils.LOAD_ALL_TODO_TODAY:
        checkRadioButton(dialogView, R.id.option_show_today);
        break;
      case PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY:
        checkRadioButton(dialogView, R.id.option_show_from_today);
        break;
      case PreferenceUtils.LOAD_ALL_TODO_TODAY_PENDING:
        checkRadioButton(dialogView, R.id.option_show_today_pending);
        break;
      case PreferenceUtils.LOAD_OVERDUE_TODO:
        checkRadioButton(dialogView, R.id.option_show_overdue);
        break;
      default:
        checkRadioButton(dialogView, R.id.option_show_from_today);
        break;
    }
  }

  private void checkRadioButton(View dialogView, int id) {
    RadioButton button = (RadioButton) dialogView.findViewById(id);
    button.setChecked(true);
  }

  private void saveSelection(int idButton) {
    switch (idButton) {
      case R.id.option_show_all:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_ALL_TODO);
        break;
      case R.id.option_show_today:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_ALL_TODO_TODAY);
        break;
      case R.id.option_show_today_pending:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_ALL_TODO_TODAY_PENDING);
        break;
      case R.id.option_show_from_today:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY);
        break;
      case R.id.option_show_overdue:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_OVERDUE_TODO);
        break;
      default:
        PreferenceUtils.saveLoadMode(m_preferences, PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY);
    }
  }
}
