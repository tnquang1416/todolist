package com.demo.android.element;


import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.demo.android.db.ToDo;
import com.demo.android.db.ToDoDB;
import com.demo.android.todolist.MainActivity;
import com.demo.android.todolist.R;
import com.demo.android.utils.CommonUtils;

/**
 * Created by Quang Tran on 27/11/2016.
 */

public class ContextActionBar implements ActionMode.Callback {
  private MainActivity m_context;
  private View m_interactedView;
  private int m_interactedViewColor;
  private ToDoDB m_database;

  public ContextActionBar(MainActivity context, View interactedView, ToDoDB database) {
    m_context = context;
    m_interactedView = interactedView;
    m_database = database;
  }

  @Override
  public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
    switch (menuItem.getItemId()) {
      case R.id.action_update:
        String id = ((TextView) m_interactedView.findViewById(R.id.tv_task_id)).getText().toString();
        String title = ((TextView) m_interactedView.findViewById(R.id.tv_task)).getText().toString();
        String description = ((TextView) m_interactedView.findViewById(R.id.tv_task_description)).getText().toString();
        String date = ((TextView) m_interactedView.findViewById(R.id.tv_date)).getText().toString();

        m_context.openInsertFragment(new ToDo(id, title, description, date));
        actionMode.finish();
        return true;
      case R.id.action_remove:
        try {
          int noRow = removeToDo();
          if (noRow > 0) {
            m_context.onViewSettingChange();
          }
        } catch (Exception ex) {
          Log.e("Remove fail", ex.getMessage());
          CommonUtils.showMessInSnackbar(m_context, ex.getMessage(), true);
        }
        actionMode.finish();
        return true;
    }
    return false;
  }

  private int removeToDo() {
    TextView idField = (TextView) m_interactedView.findViewById(R.id.tv_task_id);
    String id = idField.getText().toString();
    m_database.open();
    int result = m_database.removeToDoByID(id);
    m_database.close();

    return result;
  }

  @Override
  public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
    m_interactedViewColor = m_interactedView.getDrawingCacheBackgroundColor();
    m_interactedView.setBackgroundColor(Color.rgb(146, 216, 220));
    actionMode.getMenuInflater().inflate(R.menu.menu_context, menu);
    return true;
  }

  @Override
  public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
    return false;
  }

  @Override
  public void onDestroyActionMode(ActionMode actionMode) {
    m_interactedView.setBackgroundColor(m_interactedViewColor);
  }
}
