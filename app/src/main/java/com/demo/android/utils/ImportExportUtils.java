package com.demo.android.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.demo.android.db.ToDo;
import com.demo.android.db.ToDoDB;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 * Created by Quang Tran on 29/5/2017.
 */

public class ImportExportUtils {
  public static final String DEFAULT_EXPORTED_FILE_NAME = "exportedToDo.xml";
  public static final String DEFAULT_FILE_IN_DOWNLOAD = "storage/sdcard0/download/todo.xml";

  public static void importToDoFromXML(Activity activity, String input) throws IOException, SAXException, ParserConfigurationException {
    File file = new File(input);
    importToDoFromXML(activity, file);
  }

  public static void importToDoFromXML(Activity activity, File file) throws IOException, SAXException, ParserConfigurationException {
    CommonUtils.showMessInSnackbar(activity, "Accessing " + file.getAbsolutePath(), false);
    List<ToDo> todoList = XMLUtils.getToDoListFromXML(file);
    ToDoDB database = new ToDoDB(activity);
    database.open();
    int count = 0;
    for (int i = 0; i < todoList.size(); i++) {
      ToDo temp = todoList.get(i);
      if (!temp.isValid()) {
        CommonUtils.showMessInSnackbar(activity, temp.getTitle() + " is invalid", false);
        continue;
      }
      long rs = database.insertNewToDo(temp);
      if (rs > 0) {
        count++;
      }
    }
    CommonUtils.showMessInSnackbar(activity, "Inserted " + count + "/" + todoList.size() + " todo", false);
    database.close();
  }

  public static void exportToDoToXML(Activity activity, String output) throws ParserConfigurationException, FileNotFoundException, TransformerException {
    // get all to do from db
    ToDoDB database = new ToDoDB(activity);
    database.open();
    List<ToDo> todoList = database.getToDoToList(PreferenceUtils.LOAD_ALL_TODO, false);
    database.close();
    // save all of them into private file xml
    FileOutputStream file = activity.openFileOutput(output, Context.MODE_PRIVATE);
    XMLUtils.saveToDoInToXMLFile(todoList, file);
  }

  public static void exportToDoToXML(Activity activity, File output) throws ParserConfigurationException, IOException, TransformerException {
    // get all to do from db
    CommonUtils.showMessInSnackbar(activity, "Accessing " + output.getAbsolutePath(), false);
    ToDoDB database = new ToDoDB(activity);
    database.open();
    List<ToDo> todoList = database.getToDoToList(PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY, false);
    database.close();
    Toast.makeText(activity, "Exporting " + todoList.size() + " todo...", Toast.LENGTH_LONG).show();
    // save all of them into private file xml (content will be override)
    FileOutputStream file = new FileOutputStream(output);
    XMLUtils.saveToDoInToXMLFile(todoList, file);
  }
}
