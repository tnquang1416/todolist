package com.demo.android.utils;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.widget.TextView;

import com.demo.android.todolist.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Author Quang Tran
 * Created on 30/11/2016.
 */

public class CommonUtils {
  public static void showException(Activity activity, String message) {
    TextView msgView = (TextView) activity.findViewById(R.id.error);
    msgView.setText(message);
  }

  public static String getTodayInString() {
    return getStringFromDate(Calendar.getInstance());
  }

  public static String getTheFollowingDateInString(int noDays) {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.DATE, noDays);
    return getStringFromDate(c);
  }

  private static String getStringFromDate(Calendar c) {
    int month = c.get(Calendar.MONTH) + 1;
    String monthInString = month < 10 ? "0" + month : String.valueOf(month);
    String dateInString = c.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + c.get(Calendar.DAY_OF_MONTH) : String.valueOf(c.get(Calendar.DAY_OF_MONTH));
    return c.get(Calendar.YEAR) + "." + monthInString + "." + dateInString;
  }

  public static String getTodayInStringWithLabel() {
    Calendar c = Calendar.getInstance();
    String rs = "";
    switch (c.get(Calendar.DAY_OF_WEEK)) {
      case Calendar.SUNDAY:
        rs = "Sunday";
        break;
      case Calendar.MONDAY:
        rs = "Monday";
        break;
      case Calendar.TUESDAY:
        rs = "Tuesday";
        break;
      case Calendar.WEDNESDAY:
        rs = "Wednesday";
        break;
      case Calendar.THURSDAY:
        rs = "Thursday";
        break;
      case Calendar.FRIDAY:
        rs = "Friday";
        break;
      case Calendar.SATURDAY:
        rs = "Saturday";
        break;
    }
    return rs + "-" + getTodayInString();
  }

  public static String convertDateFromNormalFormatToDBFormat(String normal) throws ParseException {
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    Date date = format.parse(normal);
    SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy.MM.DD", Locale.US);
    return dbFormat.format(date);
  }

  public static String convertDateFromDBFormatToNormalFormat(String dbDate) throws ParseException {
    SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.US);
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    Date date = dbFormat.parse(dbDate);
    return format.format(date);
  }

  public static void showMessInSnackbar(Activity activity, String message, boolean isLong) {
    if (isLong) {
      Snackbar.make(activity.findViewById(R.id.content_main), message, Snackbar.LENGTH_LONG).show();
    } else {
      Snackbar.make(activity.findViewById(R.id.content_main), message, Snackbar.LENGTH_SHORT).show();
    }
  }
}
