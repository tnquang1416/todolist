package com.demo.android.utils;

import com.demo.android.db.ToDo;
import com.demo.android.db.ToDoDB;

/**
 * Created by QuangTran on 10/15/2017.
 */

public class ToDoUtils {
    public static long insertNewToDo(ToDoDB db, String title, String description, String date) {
        ToDo insertedToDo = new ToDo(title, description, date);
        if (!insertedToDo.isValid()) {
            return -1;
        }
        db.open();
        long result = db.insertNewToDo(insertedToDo);
        db.close();

        return result;
    }

    public static long updateOneToDo(ToDoDB db, String id, String title, String description, String date) {
        ToDo updatedToDo = new ToDo(id, title, description, date);

        if (!updatedToDo.isValid()) {
            return -1;
        }
        db.open();
        long result = db.updateToDo(updatedToDo);
        db.close();

        return result;
    }

    public static long removeOldToDo(ToDoDB db) {
        db.open();
        long rs = db.removeOldToDo();
        db.close();

        return rs;
    }

    public static long removeDoneToDo(ToDoDB db) {
        db.open();
        long rs = db.removeOldDoneToDo();
        db.close();
        
        return rs;
    }
}
