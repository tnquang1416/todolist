package com.demo.android.utils;

import com.demo.android.db.ToDoDB;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Author: Quang Tran
 * Created on 2/12/2016.
 */

public class ValidationUtils {
  public static final String WRONG_INPUT = "###";

  static public boolean validateTitle(String title) {
    if (title == null) {
      return false;
    }

    if (title.trim().isEmpty()) {
      return false;
    }

    return true;
  }

  static public String validateAndCorrectMonth(String month) {
    try {
      int m = Integer.parseInt(month);
      if (m < 1 || m > 12) {
        return validateAndCorrectMonth(Calendar.MONTH + "");
      }
      if (m < 10) {
        return "0" + m;
      }

      return month;
    } catch (Exception ex) {
      return validateAndCorrectMonth(Calendar.MONTH + "");
    }
  }

  static public String validateAndCorrectDay(String day) {
    try {
      int d = Integer.parseInt(day);
      if (d < 1 || d > 31) {
        return validateAndCorrectMonth(Calendar.DAY_OF_MONTH + "");
      }
      if (d < 10) {
        return "0" + d;
      }

      return day;
    } catch (Exception ex) {
      return validateAndCorrectMonth(Calendar.DAY_OF_MONTH + "");
    }
  }

  static public String validateAndCorrectYear(String year) {
    try {
      int y = Integer.parseInt(year);
      if (y < 1000 || y > 3000) {
        return validateAndCorrectMonth(Calendar.YEAR + "");
      }

      return year;
    } catch (Exception ex) {
      return validateAndCorrectMonth(Calendar.YEAR + "");
    }
  }

  static public String validateAndCorrect(String date) {
    if (date == null || date.isEmpty()) {
      return ToDoDB.DATE_VALUE_PENDING;
    }

    if (date.equalsIgnoreCase(ToDoDB.DATE_VALUE_PENDING)) {
      return ToDoDB.DATE_VALUE_PENDING;
    }

    SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.DD", Locale.US);
    try {
      format.parse(date);
    } catch (ParseException ex) {
      return WRONG_INPUT;
    }

    return date;
  }

}
