package com.demo.android.utils;

import android.content.SharedPreferences;

/**
 * Last updated by Quang Tran in May 11th, 2017
 */

public class PreferenceUtils {
  public static final String LOAD_MODE_LABEL = "loadMode";
  public static final String LOAD_OPTION_DONE_TODO = "loadDoneToDo";
  public static final String PREF_NAME = "ToDoItemPreferences";
  public static final int LOAD_ALL_TODO = 0;
  public static final int LOAD_ALL_TODO_TODAY = 1;
  public static final int LOAD_ALL_TODO_FROM_TODAY = 2;
  public static final int LOAD_ALL_TODO_TODAY_PENDING = 3;
  public static final int LOAD_OVERDUE_TODO = 4;
  public static final int LOAD_UNDONE_TODO_ONLY = 1;

  public static int getLoadMode(SharedPreferences preferences) {
    return preferences.getInt(LOAD_MODE_LABEL, LOAD_ALL_TODO_FROM_TODAY);
  }

  public static boolean getLoadToDoOption(SharedPreferences preferences) {
    return preferences.getInt(LOAD_OPTION_DONE_TODO, LOAD_UNDONE_TODO_ONLY) == LOAD_ALL_TODO;
  }

  public static void saveLoadMode(SharedPreferences preferences, int mode) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt(LOAD_MODE_LABEL, mode);
    editor.apply();
  }

  public static void saveLoadDoneToDoOption(SharedPreferences preferences, int isLoad) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt(LOAD_OPTION_DONE_TODO, isLoad);
    editor.apply();
  }
}
