package com.demo.android.utils;

import com.demo.android.db.ToDo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Quang Tran on 25/5/2017.
 */

public class XMLUtils {
  public static final String TO_DO_S_TAG = "ToDos";
  public static final String TO_DO_TAG = "ToDo";
  public static final String TITLE_TAG = "Title";
  public static final String DATE_TAG = "Date";
  public static final String MONTH_TAG = "Month";
  public static final String YEAR_TAG = "Year";
  public static final String DESCRIPTION_TAG = "Description";

  public static List<ToDo> getToDoListFromXML(File file) throws ParserConfigurationException, IOException, SAXException {
    List<ToDo> result = new ArrayList<>();
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(file);
    NodeList todoNodes = document.getElementsByTagName(TO_DO_TAG);

    for (int i = 0; i < todoNodes.getLength(); i++) {
      result.add(getToDoFromNode(todoNodes.item(i)));
    }

    return result;
  }

  private static ToDo getToDoFromNode(Node toDoNode) {
    String title = "";
    String day = "";
    String month = "";
    String year = "";
    String des = "";
    NodeList children = toDoNode.getChildNodes();

    for (int i = 0; i < children.getLength(); i++) {
      Node temp = children.item(i);
      switch (temp.getNodeName()) {
        case TITLE_TAG:
          title = temp.getTextContent();
          break;
        case DATE_TAG:
          day = temp.getTextContent();
          break;
        case MONTH_TAG:
          month = ValidationUtils.validateAndCorrectMonth(temp.getTextContent());
          break;
        case YEAR_TAG:
          year = ValidationUtils.validateAndCorrectYear(temp.getTextContent());
          break;
        case DESCRIPTION_TAG:
          des = temp.getTextContent();
          break;
      }
    }
    return new ToDo(title, des, day);
  }

  /**
   * Export todo but the done ones
   *
   * @param toDoList
   * @param output
   * @return
   * @throws ParserConfigurationException
   */
  public static void saveToDoInToXMLFile(List<ToDo> toDoList, FileOutputStream output) throws ParserConfigurationException, TransformerException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.newDocument();

    Element root = document.createElement(TO_DO_S_TAG);
    document.appendChild(root);

    for (ToDo todo : toDoList) {
      Element toDoElement = document.createElement(TO_DO_TAG);
      root.appendChild(toDoElement);

      Element titleElement = document.createElement(TITLE_TAG);
      titleElement.appendChild(document.createTextNode(todo.getTitle()));
      toDoElement.appendChild(titleElement);

      Element dateElement = document.createElement(DATE_TAG);
      dateElement.appendChild(document.createTextNode(todo.getDate()));
      toDoElement.appendChild(dateElement);

      Element desElement = document.createElement(DESCRIPTION_TAG);
      desElement.appendChild(document.createTextNode(todo.getDescription()));
      toDoElement.appendChild(desElement);
    }

    TransformerFactory transFactory = TransformerFactory.newInstance();
    Transformer trans = transFactory.newTransformer();
    DOMSource source = new DOMSource(document);
    StreamResult result = new StreamResult(output);
    trans.transform(source, result);
  }
}
