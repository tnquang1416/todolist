package com.demo.android.db;

import com.demo.android.utils.ValidationUtils;

/**
 * Created by Quang Tran on 25/11/2016.
 */

public class ToDo {
  private String m_id;
  private String m_title;
  private String m_description;
  private String m_date;
  private String m_state;

  public ToDo(String title, String description, String date) {
    m_id = "";
    m_title = title;
    m_description = description;
    m_date = date;
    m_state = ToDoDB.STATE_UNDONE;
  }

  public ToDo(String id, String title, String description, String date) {
    m_id = id;
    m_title = title;
    m_description = description;
    m_date = date;
    m_state = ToDoDB.STATE_UNDONE;
  }

  public ToDo(String id, String title, String description, String date, String state) {
    m_id = id;
    m_title = title;
    m_description = description;
    m_date = date;
    m_state = state;
  }

  public String getTitle() {
    return m_title;
  }

  public String getDate() {
    return m_date;
  }

  public String getDescription() {
    return m_description;
  }

  public String getID() {
    return m_id;
  }

  public String getState() {
    return m_state;
  }

  public boolean isValid() {
    return ValidationUtils.validateTitle(getTitle()) && (!getDate().equalsIgnoreCase(ValidationUtils.WRONG_INPUT));
  }

  @Override
  public String toString() {
    return getID() + " - " + getTitle() + " - " + getDate() + " - " + getState() + "\n" + getDescription();
  }
}
