package com.demo.android.db;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.demo.android.utils.CommonUtils;
import com.demo.android.utils.ImportExportUtils;

import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

/**
 * Created by Quang Tran on 25/11/2016.
 */

public class ToDoDBHelper extends SQLiteOpenHelper {
  private static final String DATABASE_NAME = "ToDoDatabase";
  private static final int DATABASE_VERSION = 6;
  private static final String TO_DO_TABLE_CREATE_QUERY = "CREATE TABLE " + ToDoDB.TABLE_NAME + "(" +
          ToDoDB.COL_TODO_TITLE + " TEXT NOT NULL," + ToDoDB.COL_TODO_DESCRIPTION + " TEXT," +
          ToDoDB.COL_TODO_DATE + " TEXT NOT NULL," + ToDoDB.COL_TODO_STATE + " TEXT," +
          ToDoDB.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT);";
  private Activity m_context;

  public ToDoDBHelper(Activity context, SQLiteDatabase.CursorFactory factory) {
    super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    m_context = context;
  }


  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(TO_DO_TABLE_CREATE_QUERY);
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    try {
      CommonUtils.showMessInSnackbar(m_context, "Exporting todo to " + m_context.getFilesDir().getAbsolutePath() + " ...", true);
      ImportExportUtils.exportToDoToXML(m_context, ImportExportUtils.DEFAULT_EXPORTED_FILE_NAME);
    } catch (Exception e) {
      e.printStackTrace();
    }
    sqLiteDatabase.execSQL("drop table if exists " + ToDoDB.TABLE_NAME);
    onCreate(sqLiteDatabase);
    // TODO: App return error: DB was locked
//    try {
//      CommonUtils.showMessInSnackbar(m_context, "Importing todo from " +
//              m_context.getFileStreamPath(ImportExportUtils.DEFAULT_EXPORTED_FILE_NAME).getAbsolutePath() + " ...", true);
//      ImportExportUtils.importToDoFromXML(m_context, ImportExportUtils.DEFAULT_EXPORTED_FILE_NAME);
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }
}
