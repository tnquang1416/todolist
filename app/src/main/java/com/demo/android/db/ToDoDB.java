package com.demo.android.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.demo.android.utils.CommonUtils;
import com.demo.android.utils.PreferenceUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Quang Tran on 25/11/2016.
 */

public class ToDoDB {
  public static final String TABLE_NAME = "demo_todo";
  public static final String COL_ID = "_id";
  private static final String COL_ID_FOR_QUERY = "rowid _id";
  public static final String COL_TODO_TITLE = "title";
  public static final String COL_TODO_DESCRIPTION = "description";
  public static final String COL_TODO_DATE = "date";
  public static final String COL_TODO_STATE = "state";

  public static final String DATE_VALUE_PENDING = "Pending";
  public static final String STATE_DONE = "Done";
  public static final String STATE_UNDONE = "UnDone";

  private SQLiteDatabase m_database;
  private ToDoDBHelper m_helper;
  private Activity m_context;

  public ToDoDB(Activity context) {
    m_context = context;
    m_helper = new ToDoDBHelper(m_context, null);
  }

  public void open() {
    try {
      m_database = m_helper.getWritableDatabase();
    } catch (Exception e) {
      Toast.makeText(m_context, e.getMessage(), Toast.LENGTH_LONG).show();
      Log.w("Read only", "Database is opened in read-only mode");
      try {
        m_database = m_helper.getReadableDatabase();
      } catch (Exception ex) {
        Toast.makeText(m_context, ex.getMessage(), Toast.LENGTH_LONG).show();
      }
    }
  }

  public void close() {
    if (m_database == null) {
      return;
    }
    m_helper.close();
  }

  public long insertNewToDo(String title, String description, String date) {
    return insertNewToDo(new ToDo(title, description, date.trim()));
  }

  public long insertNewToDo(ToDo todo) {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1L;
    }
    ContentValues values = new ContentValues();
    values.put(COL_TODO_TITLE, todo.getTitle());
    values.put(COL_TODO_DESCRIPTION, todo.getDescription());
    values.put(COL_TODO_DATE, todo.getDate());
    values.put(COL_TODO_STATE, todo.getState());

    return m_database.insert(TABLE_NAME, null, values);
  }

  public Cursor getToDo(int mode, boolean showDoneToDo) {
    String orderBy = COL_TODO_DATE + " ASC";
    StringBuilder selection = new StringBuilder();
    List<String> selectionArgs = new ArrayList<>();
    if (!showDoneToDo) {
      selection.append("( ");
      selection.append(COL_TODO_STATE).append(" <> ?");
      selection.append(" OR ");
      selection.append(COL_TODO_STATE).append(" IS NULL");
      selection.append(") ");
      selectionArgs.add(STATE_DONE);
      if (mode != PreferenceUtils.LOAD_ALL_TODO) {
        selection.append(" AND ");
      }
    }

    switch (mode) {
      // select * from ToDoTable where date >= Today or date = Pending
      case PreferenceUtils.LOAD_ALL_TODO_FROM_TODAY:
        selection.append("(");
        selection.append(COL_TODO_DATE + ">= ?");
        selection.append(" OR ");
        selection.append(COL_TODO_DATE + "= ?)");
        selectionArgs.add(CommonUtils.getTodayInString());
        selectionArgs.add(DATE_VALUE_PENDING);
        break;
      // select * from ToDoTable where date = Today
      case PreferenceUtils.LOAD_ALL_TODO_TODAY:
        selection.append(COL_TODO_DATE).append("= ?");
        selectionArgs.add(CommonUtils.getTodayInString());
        break;
      // select * from ToDoTable where date = Today or date = Pending
      case PreferenceUtils.LOAD_ALL_TODO_TODAY_PENDING:
        selection.append("(");
        selection.append(COL_TODO_DATE).append("= ? OR ");
        selection.append(COL_TODO_DATE).append("= ?)");
        selectionArgs.add(CommonUtils.getTodayInString());
        selectionArgs.add(DATE_VALUE_PENDING);
        break;
      // select * from ToDoTable where date < Today and state = unDone
      case PreferenceUtils.LOAD_OVERDUE_TODO:
        selection = new StringBuilder();
        selection.append(COL_TODO_DATE).append("< ? AND ");
        selection.append(COL_TODO_DATE).append("<> ? AND ");
        selection.append(COL_TODO_STATE).append("<> ?");
        selectionArgs.clear();
        selectionArgs.add(CommonUtils.getTodayInString());
        selectionArgs.add(DATE_VALUE_PENDING);
        selectionArgs.add(STATE_DONE);
        break;
    }

    String[] columns = new String[]{ToDoDB.COL_ID_FOR_QUERY, ToDoDB.COL_TODO_TITLE, ToDoDB.COL_TODO_DATE,
            ToDoDB.COL_TODO_DESCRIPTION, COL_TODO_STATE};

    return m_database.query(TABLE_NAME, columns, selection.toString(), selectionArgs.toArray(new String[]{}), null, null, orderBy);
  }

  public List<ToDo> getToDoToList(int mode, boolean showDoneToDo) {
    List<ToDo> result = new ArrayList<>();
    Cursor resultInCursor = null;
    try {
      resultInCursor = getToDo(mode, showDoneToDo);
    } catch (Exception ex) {
      CommonUtils.showException(m_context, Arrays.toString(ex.getStackTrace()));
    }

    if (!resultInCursor.moveToFirst()) {
      return result;
    }

    do {
      String title = resultInCursor.getString(resultInCursor.getColumnIndex(COL_TODO_TITLE));
      String description = resultInCursor.getString(resultInCursor.getColumnIndex(COL_TODO_DESCRIPTION));
      String date = resultInCursor.getString(resultInCursor.getColumnIndex(COL_TODO_DATE));
      String state = resultInCursor.getString(resultInCursor.getColumnIndex(COL_TODO_STATE));
      result.add(new ToDo(null, title, description, date, state));
    } while (resultInCursor.moveToNext());

    return result;
  }

  public int removeToDoByID(String id) {
    if (m_database == null) {
      Log.w("Cannot found db", "CThe database is null");
      return -1;
    }
    String selection = COL_ID + " = ?";
    String[] selectionArgs = {id};
    return m_database.delete(TABLE_NAME, selection, selectionArgs);
  }

  public int updateStateDone(String id) {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1;
    }

    ContentValues values = new ContentValues();
    values.put(COL_TODO_STATE, STATE_DONE);
    String selection = COL_ID + " = ?";
    String[] selectionArgs = {id};
    return m_database.update(TABLE_NAME, values, selection, selectionArgs);
  }

  public int updateStateUndone(String id) {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1;
    }

    ContentValues values = new ContentValues();
    values.put(COL_TODO_STATE, STATE_UNDONE);
    String selection = COL_ID + " =?";
    String[] selectionArgs = {id};
    return m_database.update(TABLE_NAME, values, selection, selectionArgs);
  }

  public int updateByID(String id, String title, String description, String date) {
    return updateByID(id, title, description, date, null);
  }

  private int updateByID(String id, String title, String description, String date, String state) {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1;
    }
    if (id == null) {
      Log.v("Query fail", "Cannot query by null value");
      return -1;
    }
    ContentValues values = new ContentValues();
    if (title != null) {
      values.put(ToDoDB.COL_TODO_TITLE, title);
    }
    if (description != null) {
      values.put(ToDoDB.COL_TODO_DESCRIPTION, description);
    }
    if (date != null) {
      values.put(ToDoDB.COL_TODO_DATE, date.trim());
    }
    if (state != null) {
      values.put(ToDoDB.COL_TODO_STATE, state);
    }
    String selection = COL_ID + " = ?";
    String[] selectionArgs = {id};
    return m_database.update(TABLE_NAME, values, selection, selectionArgs);
  }

  public int updateToDo(ToDo todo) {
    return updateByID(todo.getID(), todo.getTitle(), todo.getDescription(), todo.getDate(), todo.getState());
  }

  public int removeOldToDo() {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1;
    }
    String selection = COL_TODO_DATE + " < ? AND " + COL_TODO_DATE + " <> ?";
    String[] selectionArgs = {CommonUtils.getTodayInString(), DATE_VALUE_PENDING};
    return m_database.delete(TABLE_NAME, selection, selectionArgs);
  }

  public int removeOldDoneToDo() {
    if (m_database == null) {
      Log.w("Cannot found db", "The database is null");
      return -1;
    }
    StringBuilder selections = new StringBuilder();
    selections.append("(").append(COL_TODO_DATE).append(" < ? OR ").
            append(COL_TODO_DATE).append(" =? ").append(")");
    selections.append(" AND ").append(COL_TODO_STATE).append(" = ?");
    String[] selectionArgs = {CommonUtils.getTodayInString(), DATE_VALUE_PENDING, STATE_DONE};
    return m_database.delete(TABLE_NAME, selections.toString(), selectionArgs);
  }
}
