package com.demo.android.listener;

import android.app.DatePickerDialog;
import android.support.design.widget.Snackbar;
import android.widget.DatePicker;
import android.view.View;
import android.widget.EditText;

import com.demo.android.todolist.R;
import com.demo.android.utils.CommonUtils;

/**
 * Created by Quang Tran on 8/5/2017.
 */

public class DateSetListener implements DatePickerDialog.OnDateSetListener {
  private View m_dialogView;

  public DateSetListener(View dialogView) {
    m_dialogView = dialogView;
  }

  @Override
  public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
    EditText editText = (EditText) m_dialogView.findViewById(R.id.et_date);
    StringBuilder value = new StringBuilder().append(i).append(".");
    // the month will be count from 0 to 11, so it need to be increased
    i1++;
    value.append(i1 < 10 ? "0" + i1 : i1).append(".");
    value.append(i2 < 10 ? "0" + i2 : i2);
    if (editText != null) {
      editText.setText(value.toString());
    }
  }
}

