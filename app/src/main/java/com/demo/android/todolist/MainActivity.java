package com.demo.android.todolist;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.android.db.ToDo;
import com.demo.android.db.ToDoDB;
import com.demo.android.element.ContextActionBar;
import com.demo.android.element.SettingDialog;
import com.demo.android.listener.DateSetListener;
import com.demo.android.utils.CommonUtils;
import com.demo.android.utils.ImportExportUtils;
import com.demo.android.utils.PreferenceUtils;
import com.demo.android.utils.ValidationUtils;
import com.demo.android.utils.XMLUtils;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
  private DatePickerDialog.OnDateSetListener m_dateSetListener;
  private ToDoDB m_database;
  /**
   * ATTENTION: This was auto-generated to implement the App Indexing API.
   * See https://g.co/AppIndexing/AndroidStudio for more information.
   */
  private GoogleApiClient client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        openInsertFragment(null);
      }
    });

    // Prepare today field
    TextView tvToday = (TextView) findViewById(R.id.tv_today);
    tvToday.setText("Today: " + CommonUtils.getTodayInStringWithLabel());

    // prepare listview
    m_database = new ToDoDB(this);
    loadToDoListItems();
    // prepare contextual menu
    ListView listView = (ListView) findViewById(R.id.lv_todo_list);
    listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return showContextActionBar(MainActivity.this, view);
      }
    });
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        showDescriptionDialog(view);
      }
    });
    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
  }

  private void showDescriptionDialog(View view) {
    TextView title = (TextView) view.findViewById(R.id.tv_task);
    TextView id = (TextView) view.findViewById(R.id.tv_task_id);
    TextView description = (TextView) view.findViewById(R.id.tv_task_description);
    TextView state = (TextView) view.findViewById(R.id.tv_task_state);
    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
    View dialogView = MainActivity.this.getLayoutInflater().inflate(R.layout.todo_description, null);
    TextView descriptionInDialog = (TextView) dialogView.findViewById(R.id.tv_description_in_dialog);


    final String idValue = id.getText().toString();
    final String expectedStateButton = state.getText().toString().equalsIgnoreCase(ToDoDB.STATE_DONE) ? "UnDone" : "Done";

    descriptionInDialog.setText(description.getText().toString());
    builder.setTitle(title.getText());
    builder.setView(dialogView);
    builder.setPositiveButton(expectedStateButton, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        switchToDoState(idValue, !expectedStateButton.equalsIgnoreCase("Done"));
      }
    });
    builder.setNegativeButton("||>", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        delayTaskOneDay(idValue);
      }
    });
    builder.show();
  }

  private void switchToDoState(String idValue, boolean isStateDone) {
    m_database.open();
    int result = isStateDone ? m_database.updateStateUndone(idValue) : m_database.updateStateDone(idValue);
    if (result > 0) {
      loadToDoItems();
      if (isStateDone) {
        CommonUtils.showMessInSnackbar(this, "Let continue your job ^_^", true);
      } else {
        CommonUtils.showMessInSnackbar(this, "A task has been done ^_^", true);
      }
    }
    m_database.close();
  }

  private void delayTaskOneDay(String idValue) {
    String delayTime = CommonUtils.getTheFollowingDateInString(1);
    m_database.open();
    int result = m_database.updateByID(idValue, null, null, delayTime);
    if (result > 0) {
      loadToDoItems();
      CommonUtils.showMessInSnackbar(this, "The task has been delayed.", true);
    }
    m_database.close();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    boolean isLoadAllToDo = PreferenceUtils.getLoadToDoOption(getSharedPreferences(PreferenceUtils.PREF_NAME, MODE_PRIVATE));
    MenuItem loadDoneToDoOption = menu.findItem(R.id.action_show_done_todo);
    loadDoneToDoOption.setChecked(isLoadAllToDo);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    switch (id) {
      case R.id.action_settings:
        openSettingsFragment();
        return true;
      case R.id.action_insert:
        openInsertFragment(null);
        return true;
      case R.id.action_remove_done:
        removeDoneToDo();
        return true;
      case R.id.action_remove_old:
        removeOldToDo();
        return true;
      case R.id.action_show_done_todo:
        item.setChecked(!item.isChecked());
        onLoadToDoOptionChange(item.isChecked());
        return true;
      case R.id.action_import:
        openImportDialog();
        return true;
      case R.id.action_export:
        openExportDialog();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  // TODO support file chooser
  private void openImportDialog() {
    try {
      ImportExportUtils.importToDoFromXML(MainActivity.this, ImportExportUtils.DEFAULT_FILE_IN_DOWNLOAD);
      loadToDoListItems();
    } catch (Exception e) {
      CommonUtils.showException(this, e.getMessage());
    }
  }

  // TODO support file chooser
  private void openExportDialog() {
    try {
      ImportExportUtils.exportToDoToXML(MainActivity.this, new File(ImportExportUtils.DEFAULT_FILE_IN_DOWNLOAD));
    } catch (Exception e) {
      CommonUtils.showException(this, e.getMessage());
    }
  }

  private void onLoadToDoOptionChange(boolean checked) {
    int mode = checked ? PreferenceUtils.LOAD_ALL_TODO : PreferenceUtils.LOAD_UNDONE_TODO_ONLY;
    onPrefSettingChange(PreferenceUtils.LOAD_OPTION_DONE_TODO, mode);
  }

  private boolean showContextActionBar(MainActivity mainActivity, View view) {
    ContextActionBar actionbar = new ContextActionBar(mainActivity, view, m_database);
    MainActivity.this.startActionMode(actionbar);
    view.setSelected(true);

    return true;
  }

  public void showDatePicker() {
    showDatePicker(null);
  }

  //  public void showDatePicker(View view, String date) {
  public void showDatePicker(String date) {
    Calendar c = Calendar.getInstance();
    DatePickerDialog picker;

    try {
      if (date == null) {
        picker = new DatePickerDialog(this, m_dateSetListener, c.get(Calendar.YEAR),
                c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
      } else {
        String[] input = date.split("\\.");
        picker = new DatePickerDialog(this, m_dateSetListener, Integer.parseInt(input[0]),
                Integer.parseInt(input[1]) - 1, Integer.parseInt(input[2]));
      }
    } catch (Exception e) {
      picker = new DatePickerDialog(this, m_dateSetListener, c.get(Calendar.YEAR),
              c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
    }
    picker.setTitle(getBaseContext().getString(R.string.insert_date));
    picker.show();
  }

  private void loadToDoItems() {
    try {
      SharedPreferences preferences = getSharedPreferences(PreferenceUtils.PREF_NAME, MODE_PRIVATE);
      Cursor cursor = m_database.getToDo(PreferenceUtils.getLoadMode(preferences), PreferenceUtils.getLoadToDoOption(preferences));
      String[] fromColumn = new String[]{ToDoDB.COL_ID, ToDoDB.COL_TODO_TITLE, ToDoDB.COL_TODO_DATE, ToDoDB.COL_TODO_DESCRIPTION, ToDoDB.COL_TODO_STATE};
      int[] toView = {R.id.tv_task_id, R.id.tv_task, R.id.tv_date, R.id.tv_task_description, R.id.tv_task_state};
      SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.todo_item, cursor, fromColumn, toView, 0);
      ListView toDoList = (ListView) findViewById(R.id.lv_todo_list);
      toDoList.setAdapter(adapter);
    } catch (Exception ex) {
      TextView error = (TextView) findViewById(R.id.error);
      error.setText(ex.getMessage());
      Snackbar.make(findViewById(R.id.content_main), ex.getMessage(), Snackbar.LENGTH_LONG).show();
    }
  }

  private void openSettingsFragment() {
    SettingDialog dialog = new SettingDialog(this);
    dialog.show();
  }

  public void openInsertFragment(ToDo todo) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    View dialogView = this.getLayoutInflater().inflate(R.layout.todo_insert, null);
    EditText title = (EditText) dialogView.findViewById(R.id.et_title);
    EditText description = (EditText) dialogView.findViewById(R.id.et_description);
    final EditText date = (EditText) dialogView.findViewById(R.id.et_date);
    final ImageButton showDatePickerButton = (ImageButton) dialogView.findViewById(R.id.bt_date_picker);
    CheckBox skipDate = (CheckBox) dialogView.findViewById(R.id.cb_pending_date);
    skipDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
          date.setInputType(InputType.TYPE_NULL);
          showDatePickerButton.setEnabled(false);
          date.setText("");
        } else {
          date.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
          date.setText(CommonUtils.getTodayInString());
          showDatePickerButton.setEnabled(true);
        }
      }
    });
    if (todo != null) {
      // open update fragment
      openUpdateFragment(todo, title, description, date, showDatePickerButton, skipDate, builder);
    } else {
      // open insert fragment
      openInsertFragment(title, description, date, showDatePickerButton, builder);
    }
    builder.setView(dialogView);
    builder.show();
    // prepare listener for picker
    m_dateSetListener = new DateSetListener(dialogView);
  }

  private void openInsertFragment(final EditText title, final EditText description, final EditText date,
                                  ImageButton showDatePickerButton, AlertDialog.Builder builder) {
    date.setText(CommonUtils.getTodayInString());
    showDatePickerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
//        showDatePicker(view, null);
        showDatePicker();
      }
    });
    builder.setPositiveButton(R.string.action_insert, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        insertToDo(title.getText().toString(), description.getText().toString(), ValidationUtils.validateAndCorrect(date.getText().toString()));
      }
    });
    builder.setNegativeButton(R.string.action_cancel, null);
  }

  private void openUpdateFragment(ToDo todo, final EditText title, final EditText description, final EditText date,
                                  ImageButton showDatePickerButton, CheckBox skipDate, AlertDialog.Builder builder) {
    final String dateInToDo = todo.getDate().equals(ToDoDB.DATE_VALUE_PENDING) ? "" : todo.getDate();
    title.setText(todo.getTitle());
    description.setText(todo.getDescription());
    if (!dateInToDo.isEmpty()) {
      date.setText(dateInToDo);
    } else {
      date.setInputType(InputType.TYPE_NULL);
      showDatePickerButton.setEnabled(false);
      date.setText("");
      skipDate.setChecked(true);
    }
    showDatePickerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showDatePicker(dateInToDo);
      }
    });

    final String id = todo.getID();
    builder.setPositiveButton(R.string.action_update, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        updateToDo(id, title.getText().toString(), description.getText().toString(), ValidationUtils.validateAndCorrect(date.getText().toString()));
      }
    });
    builder.setNegativeButton(R.string.action_cancel, null);
  }

  private void insertToDo(String title, String description, String date) {
    try {
      // Get latest value of components in dialog's view
      ToDo insertedToDo = new ToDo(title, description, date);
      if (!insertedToDo.isValid()) {
        CommonUtils.showMessInSnackbar(MainActivity.this, "Invalid input. Skipping update.", true);
        return;
      }
      m_database.open();
      long result = m_database.insertNewToDo(insertedToDo);
      m_database.close();
      if (result > 0) {
        CommonUtils.showMessInSnackbar(MainActivity.this, "Insert successful", false);
        loadToDoListItems();
      }
    } catch (Exception ex) {
      Log.e("Insert Fail", ex.getMessage());
      CommonUtils.showMessInSnackbar(MainActivity.this, ex.getMessage(), true);
    }
  }

  private void updateToDo(String id, String title, String description, String date) {
    try {
      // Get latest value of components in dialog's view
      ToDo updatedToDo = new ToDo(id, title, description, date);

      if (!updatedToDo.isValid()) {
        CommonUtils.showMessInSnackbar(MainActivity.this, "Invalid input. Skipping update.\n" + updatedToDo.getTitle() + " " + updatedToDo.getDate(), true);
        return;
      }

      m_database.open();
      long result = m_database.updateToDo(updatedToDo);
      m_database.close();
      if (result > 0) {
        CommonUtils.showMessInSnackbar(MainActivity.this, "Update successful", false);
        loadToDoListItems();
      }
    } catch (Exception ex) {
      Log.e("Update Fail", ex.getMessage());
      CommonUtils.showMessInSnackbar(MainActivity.this, ex.getMessage(), true);
    }
  }

  public void loadToDoListItems() {
    m_database.open();
    loadToDoItems();
    m_database.close();
  }

  public void onViewSettingChange() {
    loadToDoListItems();
  }

  public void onPrefSettingChange(String type, int value) {
    SharedPreferences pre = getSharedPreferences(PreferenceUtils.PREF_NAME, MODE_PRIVATE);
    if (type.equalsIgnoreCase(PreferenceUtils.LOAD_MODE_LABEL)) {
      PreferenceUtils.saveLoadMode(pre, value);
    } else if (type.equalsIgnoreCase(PreferenceUtils.LOAD_OPTION_DONE_TODO)) {
      PreferenceUtils.saveLoadDoneToDoOption(pre, value);
    }
    onViewSettingChange();
  }

  private void removeOldToDo() {
    try {
      m_database.open();
      m_database.removeOldToDo();
      loadToDoItems();
      m_database.close();
      CommonUtils.showMessInSnackbar(MainActivity.this, "Old ToDo was removed", false);
    } catch (Exception ex) {
//      CommonUtils.showException(MainActivity.this, ex.getMessage());
      CommonUtils.showMessInSnackbar(MainActivity.this, ex.getMessage(), true);
    }
  }

  // remove old and done to do
  private void removeDoneToDo() {
    try {
      m_database.open();
      m_database.removeOldDoneToDo();
      loadToDoItems();
      m_database.close();
      CommonUtils.showMessInSnackbar(MainActivity.this, "Done ToDo was removed", false);
    } catch (Exception ex) {
//      CommonUtils.showException(MainActivity.this, ex.getMessage());
      CommonUtils.showMessInSnackbar(MainActivity.this, ex.getMessage(), true);
    }
  }

  /**
   * ATTENTION: This was auto-generated to implement the App Indexing API.
   * See https://g.co/AppIndexing/AndroidStudio for more information.
   */
  public Action getIndexApiAction() {
    Thing object = new Thing.Builder()
            .setName("Main Page") // TODO: Define a title for the content shown.
            // TODO: Make sure this auto-generated URL is correct.
            .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
            .build();
    return new Action.Builder(Action.TYPE_VIEW)
            .setObject(object)
            .setActionStatus(Action.STATUS_TYPE_COMPLETED)
            .build();
  }

  @Override
  public void onStart() {
    super.onStart();

    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client.connect();
    AppIndex.AppIndexApi.start(client, getIndexApiAction());
  }

  @Override
  public void onStop() {
    super.onStop();

    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    AppIndex.AppIndexApi.end(client, getIndexApiAction());
    client.disconnect();
  }
}
